CREATE TABLE IF NOT EXISTS games (
    id bigserial PRIMARY KEY,
    name text NOT NULL,
    version integer NOT NULL DEFAULT 1,
    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW(),
    UNIQUE(name)
);

CREATE TABLE IF NOT EXISTS leaderboards (
    id bigserial PRIMARY KEY,
    game_id bigint NOT NULL REFERENCES games ON DELETE CASCADE,
    name text NOT NULL,
    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW(),
    version integer NOT NULL DEFAULT 1,
    UNIQUE(game_id, name)
);

CREATE TABLE IF NOT EXISTS scores (
    id bigserial PRIMARY KEY,
    leaderboard_id bigint NOT NULL REFERENCES leaderboards ON DELETE CASCADE,
    username text NOT NULL,
    value text NOT NULL,
    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW(),
    version integer NOT NULL DEFAULT 1,
    UNIQUE(leaderboard_id, username)
);