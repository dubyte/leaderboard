package data

import (
	"database/sql"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_GameModel_Create(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("INSERT INTO games").
			WillReturnRows(mock.NewRows([]string{"ID", "created_at", "version"}).
				AddRow(1, time.Now(), 1))
		require.NoError(t, err)
		model := GameModel{DB: db}
		g := Game{Name: "test"}

		// Act
		err = model.Create(&g)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, 1, g.ID)
		assert.EqualValues(t, 1, g.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("name already exists", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		pqErr := &pq.Error{Code: pq.ErrorCode("23505")}
		mock.ExpectQuery("INSERT INTO games").WillReturnError(pqErr)
		require.NoError(t, err)
		model := GameModel{DB: db}
		g := Game{Name: "test"}

		// Act
		err = model.Create(&g)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return un expected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("INSERT INTO games").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := GameModel{DB: db}
		g := Game{Name: "test"}

		// Act
		err = model.Create(&g)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_GameModel_Read(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, name, version, created_at").
			WillReturnRows(mock.NewRows([]string{"ID", "name", "version", "created_at"}).
				AddRow(1, "testgame", 1, time.Now()))
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		g, err := model.Read(1)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, g.ID, 1)
		assert.Equal(t, "testgame", g.Name)
		assert.EqualValues(t, 1, g.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("id passed as argument is 0", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		_, err = model.Read(0)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("id passed as argument is -1", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		_, err = model.Read(-1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return no rows", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, name, version, created_at").
			WillReturnError(sql.ErrNoRows)
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		_, err = model.Read(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, name, version, created_at").
			WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		_, err = model.Read(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_GameModel_Update(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("UPDATE games").
			WillReturnRows(mock.NewRows([]string{"version"}).
				AddRow(2))
		require.NoError(t, err)
		model := GameModel{DB: db}
		g := Game{ID: 1, Name: "test", Version: 1}

		// Act
		err = model.Update(&g)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, 1, g.ID)
		assert.Equal(t, "test", g.Name)
		assert.EqualValues(t, 2, g.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("name already exists", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		pqErr := &pq.Error{Code: pq.ErrorCode("23505")}
		mock.ExpectQuery("UPDATE games").WillReturnError(pqErr)
		require.NoError(t, err)
		model := GameModel{DB: db}
		g := Game{ID: 1, Name: "test", Version: 1}

		// Act
		err = model.Update(&g)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return un expected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("UPDATE games").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := GameModel{DB: db}
		g := Game{ID: 1, Name: "test", Version: 1}

		// Act
		err = model.Update(&g)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_GameModel_Delete(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM games").WillReturnResult(sqlmock.NewResult(0, 1))
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.NoError(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("passed argument is negative", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		err = model.Delete(-1)

		// Assert
		require.Error(t, err)
		require.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("passed argument is zero", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		err = model.Delete(0)

		// Assert
		require.Error(t, err)
		require.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("Exec return un expeced error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM games").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("result affectedRows call return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM games").WillReturnResult(sqlmock.NewErrorResult(assert.AnError))
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("no affected rows", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM games").WillReturnResult(sqlmock.NewResult(0, 0))
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		assert.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_GameModel_List(t *testing.T) {
	buildFilters := func() Filters {
		return Filters{
			Page:         1,
			PageSize:     1,
			Sort:         "id",
			SortSafelist: []string{"id", "name", "-id", "-name"},
		}
	}
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), id, name, version, created_at").
			WillReturnRows(sqlmock.NewRows([]string{"count", "id", "name", "version", "created_at"}).
				AddRow(1, 1, "test", 1, time.Now()))
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		results, _, err := model.List("", buildFilters())

		// Assert
		require.NoError(t, err)
		require.Len(t, results, 1)
		assert.EqualValues(t, 1, results[0].ID)
		assert.Equal(t, "test", results[0].Name)
		assert.EqualValues(t, 1, results[0].Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), id, name, version, created_at").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		_, _, err = model.List("", buildFilters())

		// Assert
		require.Error(t, err)

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("scan error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), id, name, version, created_at").
			WillReturnRows(sqlmock.NewRows([]string{"id", "name", "version", "created_at"}).
				AddRow(1, "test", 1, "hello")) // force scan error by returning string hello when a time.Time was expected
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		_, _, err = model.List("", buildFilters())

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("Iteration error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), id, name, version, created_at").
			WillReturnRows(sqlmock.NewRows([]string{"id", "name", "version", "created_at"}).
				AddRow(1, "test", 1, time.Now()).RowError(0, assert.AnError))
		require.NoError(t, err)
		model := GameModel{DB: db}

		// Act
		_, _, err = model.List("", buildFilters())

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
