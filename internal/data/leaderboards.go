package data

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"
	"unicode/utf8"

	"github.com/lib/pq"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

type LeaderboardModel struct {
	DB *sql.DB
}

const LeaderboardNameMaxLen = 25

type Leaderboard struct {
	ID        int       `json:"-"`
	Name      string    `json:"name"`
	GameID    int       `json:"game_id"`
	CreatedAt time.Time `json:"-"`
	Version   int       `json:"version"`
}

// Create inserts into Leaderboard table the content of the passed value.
func (m LeaderboardModel) Create(l *Leaderboard) error {
	query := `
	INSERT INTO leaderboards (name, game_id)
	VALUES ($1, $2)
	RETURNING ID, version, created_at`

	args := []any{l.Name, l.GameID}

	err := m.DB.QueryRow(query, args...).Scan(&l.ID, &l.Version, &l.CreatedAt) //nolint: execinquery
	if err != nil {
		var pqError *pq.Error
		switch {
		case errors.As(err, &pqError) && pqError.Code.Name() == "unique_violation":
			return ErrEditConflict
		default:
			return err
		}
	}

	return nil
}

// Read return a leaderboard that was found using the id passed as argument.
func (m LeaderboardModel) Read(id int) (*Leaderboard, error) {
	if id < 1 {
		return nil, ErrRecordNotFound
	}

	query := `
	SELECT id, name, game_id, version, created_at
	FROM leaderboards
	WHERE id = $1`

	var leaderboard Leaderboard

	err := m.DB.QueryRow(query, id).
		Scan(&leaderboard.ID, &leaderboard.Name, &leaderboard.GameID, &leaderboard.Version, &leaderboard.CreatedAt)

	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, ErrRecordNotFound
		default:
			return nil, err
		}
	}

	return &leaderboard, nil
}

// Update receives a leaderboard value and it is used to update a record.
func (m LeaderboardModel) Update(l *Leaderboard) error {
	query := `
	UPDATE leaderboards
	SET name = $1, version = version + 1
	WHERE id = $2
	RETURNING version`

	args := []any{l.Name, l.ID}

	err := m.DB.QueryRow(query, args...).Scan(&l.Version) //nolint: execinquery
	if err != nil {
		var pqError *pq.Error
		switch {
		case errors.As(err, &pqError) && pqError.Code.Name() == "unique_violation":
			return ErrEditConflict
		default:
			return err
		}
	}

	return err
}

// Delete use the passed id to delete the row from leaderboards table with that ID.
func (m LeaderboardModel) Delete(id int) error {
	if id < 1 {
		return ErrRecordNotFound
	}

	query := `
	DELETE FROM leaderboards
	WHERE id = $1`

	result, err := m.DB.Exec(query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrRecordNotFound
	}

	return nil
}

// List return the leaderboards
func (m LeaderboardModel) List(name string, gameID int, filters Filters) ([]*Leaderboard, Metadata, error) {
	// Perform a paginated full-text search on the 'l;eaderboards' table, sorting by the specified column and direction.
	//nolint: gosec
	query := fmt.Sprintf(`
        SELECT count(*) OVER(), l.id, l.name, l.game_id, l.version, l.created_at
        FROM leaderboards AS l
		JOIN games AS g
		ON l.game_id = g.id
        WHERE g.id = $1
		AND (to_tsvector('simple', l.name) @@ plainto_tsquery('simple', $2) OR $2 = '')      
        ORDER BY %s %s, l.id ASC
        LIMIT $3 OFFSET $4`, filters.sortColumn(), filters.sortDirection())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	args := []any{gameID, name, filters.limit(), filters.offset()}

	rows, err := m.DB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, Metadata{}, err
	}
	defer rows.Close()

	var totalRecords int
	leaderboards := make([]*Leaderboard, 0)
	for rows.Next() {
		var leaderboard Leaderboard
		err = rows.Scan(&totalRecords, &leaderboard.ID, &leaderboard.Name, &leaderboard.GameID, &leaderboard.Version, &leaderboard.CreatedAt)
		if err != nil {
			return nil, Metadata{}, err
		}

		leaderboards = append(leaderboards, &leaderboard)
	}

	// check if there was any error during iteration.
	if err = rows.Err(); err != nil {
		return nil, Metadata{}, err
	}

	metadata := calculateMetadata(totalRecords, filters.Page, filters.PageSize)

	return leaderboards, metadata, nil
}

// ValidateLeaderboard check the fields from the passed leaderboard.
func ValidateLeaderboard(v *validator.Validator, l *Leaderboard) {
	v.Check(l.Name != "", "name", "can not be empyt")
	v.Check(utf8.RuneCountInString(l.Name) <= LeaderboardNameMaxLen, "name", fmt.Sprintf("name should not exceed %d characters", LeaderboardNameMaxLen))
}
