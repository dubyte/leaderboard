package data

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"
	"unicode/utf8"

	"github.com/lib/pq"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

type ScoreModel struct {
	DB *sql.DB
}

type Score struct {
	ID            int       `json:"-"`
	LeaderboardID int       `json:"leaderboard_id"`
	Username      string    `json:"username"`
	Value         string    `json:"value"`
	Version       int       `json:"version"`
	CreatedAt     time.Time `json:"-"`
}

const scoreValueMaxLen = 12
const ScoreUsernameMaxLen = 12

// Create inserts into Leaderboard table the content of the passed value.
func (m ScoreModel) Create(s *Score) error {
	query := `
	INSERT INTO scores (username, leaderboard_id, Value)
	VALUES ($1, $2, $3)
	RETURNING ID, version, created_at`

	args := []any{s.Username, s.LeaderboardID, s.Value}

	err := m.DB.QueryRow(query, args...).Scan(&s.ID, &s.Version, &s.CreatedAt) //nolint: execinquery
	if err != nil {
		var pqError *pq.Error
		switch {
		case errors.As(err, &pqError) && pqError.Code.Name() == "unique_violation":
			return ErrEditConflict
		default:
			return err
		}
	}

	return nil
}

// Read return a leaderboard that was found using the id passed as argument.
func (m ScoreModel) Read(id int) (*Score, error) {
	if id < 1 {
		return nil, ErrRecordNotFound
	}

	query := `
	SELECT id, username, leaderboard_id, value, version, created_at
	FROM scores
	WHERE id = $1`

	var s Score

	err := m.DB.QueryRow(query, id).
		Scan(&s.ID, &s.Username, &s.LeaderboardID, &s.Value, &s.Version, &s.CreatedAt)

	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, ErrRecordNotFound
		default:
			return nil, err
		}
	}

	return &s, nil
}

// Update receives a leaderboard value and it is used to update a record.
// it sets the version on the passed score
func (m ScoreModel) Update(s *Score) error {
	query := `
	UPDATE scores
	SET value = $1, version = version + 1
	WHERE id = $2
	RETURNING version`

	args := []any{s.Value, s.ID}

	err := m.DB.QueryRow(query, args...).Scan(&s.Version) //nolint: execinquery
	if err != nil {
		var pqError *pq.Error
		switch {
		case errors.As(err, &pqError) && pqError.Code.Name() == "unique_violation":
			return ErrEditConflict
		default:
			return err
		}
	}

	return err
}

// Delete use the passed id to delete the row from leaderboards table with that ID.
func (m ScoreModel) Delete(id int) error {
	if id < 1 {
		return ErrRecordNotFound
	}

	query := `
	DELETE FROM scores
	WHERE id = $1`

	result, err := m.DB.Exec(query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrRecordNotFound
	}

	return nil
}

// List return the scores
func (m ScoreModel) List(leaderboardID int, filters Filters) ([]*Score, Metadata, error) {
	// Perform a paginated full-text search on the 'scores' table, sorting by the specified column and direction.
	//nolint: gosec
	query := fmt.Sprintf(`
        SELECT count(*) OVER(), s.id, s.username, s.leaderboard_id, s.value, s.version, s.created_at
        FROM scores AS s
		JOIN leaderboards AS l
		ON s.leaderboard_id = l.id
        WHERE l.id = $1
        ORDER BY %s %s, s.id ASC
        LIMIT $2 OFFSET $3`, filters.sortColumn(), filters.sortDirection())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	args := []any{leaderboardID, filters.limit(), filters.offset()}

	rows, err := m.DB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, Metadata{}, err
	}
	defer rows.Close()

	var totalRecords int
	scores := make([]*Score, 0)
	for rows.Next() {
		var s Score
		err = rows.Scan(&totalRecords, &s.ID, &s.Username, &s.LeaderboardID, &s.Value, &s.Version, &s.CreatedAt)
		if err != nil {
			return nil, Metadata{}, err
		}

		scores = append(scores, &s)
	}

	// check if there was any error during iteration.
	if err = rows.Err(); err != nil {
		return nil, Metadata{}, err
	}

	metadata := calculateMetadata(totalRecords, filters.Page, filters.PageSize)

	return scores, metadata, nil
}

func ValidateScore(v *validator.Validator, score *Score) {
	v.Check(score.Username != "", "username", "must be provided")
	v.Check(utf8.RuneCountInString(score.Username) <= ScoreUsernameMaxLen, "value", fmt.Sprintf("name should not exceed %d characters", ScoreUsernameMaxLen))

	v.Check(score.Value != "", "value", "must be provided")
	v.Check(utf8.RuneCountInString(score.Value) <= scoreValueMaxLen, "value", fmt.Sprintf("name should not exceed %d characters", scoreValueMaxLen))
}
