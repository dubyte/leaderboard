package data

import (
	"database/sql"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_ScoreModel_Create(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("INSERT INTO scores").
			WillReturnRows(mock.NewRows([]string{"ID", "version", "created_at"}).
				AddRow(1, 1, time.Now()))
		require.NoError(t, err)
		model := ScoreModel{DB: db}
		s := Score{ID: 1, Username: "test", LeaderboardID: 1}

		// Act
		err = model.Create(&s)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, 1, s.ID)
		assert.EqualValues(t, 1, s.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("name already exists", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		pqErr := &pq.Error{Code: pq.ErrorCode("23505")}
		mock.ExpectQuery("INSERT INTO scores").WillReturnError(pqErr)
		require.NoError(t, err)
		model := ScoreModel{DB: db}
		s := Score{ID: 1, Username: "test", LeaderboardID: 1}

		// Act
		err = model.Create(&s)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return un expected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("INSERT INTO scores").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := ScoreModel{DB: db}
		s := Score{ID: 1, Username: "test", LeaderboardID: 1}

		// Act
		err = model.Create(&s)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_ScoreModel_Read(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, username, leaderboard_id, value, version, created_at").
			WillReturnRows(mock.NewRows([]string{"ID", "username", "leaderboard_id", "value", "version", "created_at"}).
				AddRow(1, "test", 1, "100", 1, time.Now()))
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		l, err := model.Read(1)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, l.ID, 1)
		assert.Equal(t, "test", l.Username)
		assert.EqualValues(t, "100", l.Value)
		assert.EqualValues(t, 1, l.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("id passed as argument is 0", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		_, err = model.Read(0)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("id passed as argument is -1", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		_, err = model.Read(-1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return no rows", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, username, leaderboard_id, value, version, created_at").
			WillReturnError(sql.ErrNoRows)
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		_, err = model.Read(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, username, leaderboard_id, value, version, created_at").
			WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		_, err = model.Read(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_ScoreModel_Update(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("UPDATE scores").
			WillReturnRows(mock.NewRows([]string{"version"}).
				AddRow(2))
		require.NoError(t, err)
		model := ScoreModel{DB: db}
		s := Score{ID: 1, Username: "test", LeaderboardID: 1}

		// Act
		err = model.Update(&s)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, 1, s.ID)
		assert.Equal(t, "test", s.Username)
		assert.EqualValues(t, 2, s.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("name already exists", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		pqErr := &pq.Error{Code: pq.ErrorCode("23505")}
		mock.ExpectQuery("UPDATE scores").WillReturnError(pqErr)
		require.NoError(t, err)
		model := ScoreModel{DB: db}
		s := Score{ID: 1, Username: "test", LeaderboardID: 1}

		// Act
		err = model.Update(&s)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return un expected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("UPDATE scores").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := ScoreModel{DB: db}
		s := Score{ID: 1, Username: "test", LeaderboardID: 1}

		// Act
		err = model.Update(&s)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_ScoreModel_Delete(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM scores").WillReturnResult(sqlmock.NewResult(0, 1))
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.NoError(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("passed argument is negative", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		err = model.Delete(-1)

		// Assert
		require.Error(t, err)
		require.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("passed argument is zero", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		err = model.Delete(0)

		// Assert
		require.Error(t, err)
		require.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("Exec return un expeced error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM scores").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("result affectedRows call return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM scores").WillReturnResult(sqlmock.NewErrorResult(assert.AnError))
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("no affected rows", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM scores").WillReturnResult(sqlmock.NewResult(0, 0))
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		assert.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_ScoreModel_List(t *testing.T) {
	buildFilters := func() Filters {
		return Filters{
			Page:         1,
			PageSize:     1,
			Sort:         "id",
			SortSafelist: []string{"id", "value", "-id", "-value"},
		}
	}

	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), s.id, s.username, s.leaderboard_id, s.value, s.version, s.created_at").
			WillReturnRows(sqlmock.NewRows([]string{"total_records", "id", "username", "leaderboard_id", "value", "version", "created_at"}).
				AddRow(1, 1, "test", 1, "100", 1, time.Now()))
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		results, _, err := model.List(1, buildFilters())

		// Assert
		require.NoError(t, err)
		require.Len(t, results, 1)
		assert.EqualValues(t, 1, results[0].ID)
		assert.Equal(t, "test", results[0].Username)
		assert.Equal(t, "100", results[0].Value)
		assert.EqualValues(t, 1, results[0].Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), s.id, s.username, s.leaderboard_id, s.value, s.version, s.created_at").
			WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		_, _, err = model.List(1, buildFilters())

		// Assert
		require.Error(t, err)

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("scan error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), s.id, s.username, s.leaderboard_id, s.value, s.version, s.created_at").
			WillReturnRows(sqlmock.NewRows([]string{"total_records", "id", "username", "leaderboard_id", "value", "version", "created_at"}).
				AddRow(1, 1, "test", 1, "100", 1, "hello")) // force scan error by returning string hello when a time.Time was expected
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		_, _, err = model.List(1, buildFilters())

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("Iteration error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), s.id, s.username, s.leaderboard_id, s.value, s.version, s.created_at").
			WillReturnRows(sqlmock.NewRows([]string{"id", "username", "leaderboard_id", "value", "version", "created_at"}).
				AddRow(1, "test", 1, "100", 1, time.Now()).RowError(0, assert.AnError))
		require.NoError(t, err)
		model := ScoreModel{DB: db}

		// Act
		_, _, err = model.List(1, buildFilters())

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
