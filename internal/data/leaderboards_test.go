package data

import (
	"database/sql"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_LeaderboardModel_Create(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("INSERT INTO leaderboards").
			WillReturnRows(mock.NewRows([]string{"ID", "version", "created_at"}).
				AddRow(1, 1, time.Now()))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}
		l := Leaderboard{Name: "test", GameID: 1}

		// Act
		err = model.Create(&l)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, 1, l.ID)
		assert.EqualValues(t, 1, l.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("name already exists", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		pqErr := &pq.Error{Code: pq.ErrorCode("23505")}
		mock.ExpectQuery("INSERT INTO leaderboards").WillReturnError(pqErr)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}
		l := Leaderboard{Name: "test", GameID: 1}

		// Act
		err = model.Create(&l)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return un expected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("INSERT INTO leaderboards").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}
		l := Leaderboard{Name: "test", GameID: 1}

		// Act
		err = model.Create(&l)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_LeaderboardModel_Read(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, name, game_id, version, created_at").
			WillReturnRows(mock.NewRows([]string{"ID", "name", "game_id", "version", "created_at"}).
				AddRow(1, "testleaderboard", 1, 1, time.Now()))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		l, err := model.Read(1)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, l.ID, 1)
		assert.Equal(t, "testleaderboard", l.Name)
		assert.EqualValues(t, 1, l.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("id passed as argument is 0", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		_, err = model.Read(0)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("id passed as argument is -1", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		_, err = model.Read(-1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return no rows", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, name, game_id, version, created_at").
			WillReturnError(sql.ErrNoRows)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		_, err = model.Read(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT id, name, game_id, version, created_at").
			WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		_, err = model.Read(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_LeaderboardModel_Update(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("UPDATE leaderboards").
			WillReturnRows(mock.NewRows([]string{"version"}).
				AddRow(2))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}
		l := Leaderboard{ID: 1, Name: "test", Version: 1}

		// Act
		err = model.Update(&l)

		// Assert
		require.NoError(t, err)
		assert.Equal(t, 1, l.ID)
		assert.Equal(t, "test", l.Name)
		assert.EqualValues(t, 2, l.Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("name already exists", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		pqErr := &pq.Error{Code: pq.ErrorCode("23505")}
		mock.ExpectQuery("UPDATE leaderboards").WillReturnError(pqErr)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}
		l := Leaderboard{ID: 1, Name: "test", Version: 1}

		// Act
		err = model.Update(&l)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return un expected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("UPDATE leaderboards").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}
		l := Leaderboard{ID: 1, Name: "test", Version: 1}

		// Act
		err = model.Update(&l)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_LeaderboardModel_Delete(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM leaderboards").WillReturnResult(sqlmock.NewResult(0, 1))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.NoError(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("passed argument is negative", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		err = model.Delete(-1)

		// Assert
		require.Error(t, err)
		require.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("passed argument is zero", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		err = model.Delete(0)

		// Assert
		require.Error(t, err)
		require.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("Exec return un expeced error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM leaderboards").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("result affectedRows call return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM leaderboards").WillReturnResult(sqlmock.NewErrorResult(assert.AnError))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("no affected rows", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectExec("DELETE FROM leaderboards").WillReturnResult(sqlmock.NewResult(0, 0))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		err = model.Delete(1)

		// Assert
		require.Error(t, err)
		assert.EqualError(t, err, "record not found")
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}

func Test_LeaderboardModel_List(t *testing.T) {
	buildFilters := func() Filters {
		return Filters{
			Page:         1,
			PageSize:     1,
			Sort:         "id",
			SortSafelist: []string{"id", "name", "-id", "-name"},
		}
	}

	t.Run("happy path", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), l.id, l.name, l.game_id, l.version, l.created_at").
			WillReturnRows(sqlmock.NewRows([]string{"total", "id", "name", "game_id", "version", "created_at"}).
				AddRow(1, 1, "test", 1, 1, time.Now()))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		results, _, err := model.List("", 1, buildFilters())

		// Assert
		require.NoError(t, err)
		require.Len(t, results, 1)
		assert.EqualValues(t, 1, results[0].ID)
		assert.Equal(t, "test", results[0].Name)
		assert.EqualValues(t, 1, results[0].Version)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("query return unexpected error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), l.id, l.name, l.game_id, l.version, l.created_at").WillReturnError(assert.AnError)
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		_, _, err = model.List("", 1, buildFilters())

		// Assert
		require.Error(t, err)

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("scan error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), l.id, l.name, l.game_id, l.version, l.created_at").
			WillReturnRows(sqlmock.NewRows([]string{"id", "name", "game_id", "version", "created_at"}).
				AddRow(1, "test", 1, 1, "hello")) // force scan error by returning string hello when a time.Time was expected
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		_, _, err = model.List("", 1, buildFilters())

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})

	t.Run("Iteration error", func(t *testing.T) {
		// Setup
		db, mock, err := sqlmock.New()
		mock.ExpectQuery("SELECT count\\(\\*\\) OVER\\(\\), l.id, l.name, l.game_id, l.version, l.created_at").
			WillReturnRows(sqlmock.NewRows([]string{"id", "name", "version", "created_at"}).
				AddRow(1, "test", 1, time.Now()).RowError(0, assert.AnError))
		require.NoError(t, err)
		model := LeaderboardModel{DB: db}

		// Act
		_, _, err = model.List("", 1, buildFilters())

		// Assert
		require.Error(t, err)
		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("there were unfulfilled expectations: %s", err)
		}
	})
}
