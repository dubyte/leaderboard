package data

import (
	"database/sql"
	"errors"
)

var (
	ErrRecordNotFound = errors.New("record not found")
	ErrEditConflict   = errors.New("edit conflict")
)

// Models contains the CRUDL models for Games and Leaderboards tables
type Models struct {
	Games        GamesCRUDL
	Leaderboards LeaderboardsCRUDL
	Scores       ScoresCRUDL
}

// GamesCRUDL is a CRUDL interface for games
type GamesCRUDL interface {
	Create(game *Game) error
	Read(id int) (*Game, error)
	Update(game *Game) error
	Delete(id int) error
	List(name string, filters Filters) ([]*Game, Metadata, error)
}

// LeaderboardsCRUDL is a CRUDL interface for leaderboards
type LeaderboardsCRUDL interface {
	Create(leaderboard *Leaderboard) error
	Read(id int) (*Leaderboard, error)
	Update(leaderboard *Leaderboard) error
	Delete(id int) error
	List(name string, gameID int, filters Filters) ([]*Leaderboard, Metadata, error)
}

type ScoresCRUDL interface {
	Create(score *Score) error
	Read(id int) (*Score, error)
	Update(score *Score) error
	Delete(id int) error
	List(leaderboardID int, filters Filters) ([]*Score, Metadata, error)
}

// NewModels return a value with Games model and Leaderboard models.
// This models are used to do CRUDL operation with the respective tables.
func NewModels(db *sql.DB) Models {
	return Models{
		Games:        GameModel{DB: db},
		Leaderboards: LeaderboardModel{DB: db},
		Scores:       ScoreModel{DB: db},
	}
}
