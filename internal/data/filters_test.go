package data

import (
	"testing"

	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func TestValidateFilters(t *testing.T) {
	tests := []struct {
		name    string
		filters Filters
		want    map[string]string
	}{
		{
			name: "valid filters",
			filters: Filters{
				Page:         1,
				PageSize:     10,
				Sort:         "name",
				SortSafelist: []string{"name", "created_at"},
			},
			want: map[string]string{},
		},
		{
			name: "invalid page",
			filters: Filters{
				Page:         0,
				PageSize:     10,
				Sort:         "name",
				SortSafelist: []string{"name", "created_at"},
			},
			want: map[string]string{"page": "must be greater than zero"},
		},
		{
			name: "page too large",
			filters: Filters{
				Page:         10000001,
				PageSize:     10,
				Sort:         "name",
				SortSafelist: []string{"name", "created_at"},
			},
			want: map[string]string{"page": "must be a maximum of 10 million"},
		},
		{
			name: "invalid page size",
			filters: Filters{
				Page:         1,
				PageSize:     0,
				Sort:         "name",
				SortSafelist: []string{"name", "created_at"},
			},
			want: map[string]string{"page_size": "must be greater than zero"},
		},
		{
			name: "page size too large",
			filters: Filters{
				Page:         1,
				PageSize:     101,
				Sort:         "name",
				SortSafelist: []string{"name", "created_at"},
			},
			want: map[string]string{"page_size": "must be a maximum of 100"},
		},
		{
			name: "invalid sort value",
			filters: Filters{
				Page:         1,
				PageSize:     10,
				Sort:         "invalid",
				SortSafelist: []string{"name", "created_at"},
			},
			want: map[string]string{"sort": "invalid sort value"},
		},
		// Add more test cases as needed.
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Setup: Create a new Validator instance with an empty Errors map.
			v := validator.New()

			// Act: Validate the filters using the ValidateFilters function.
			ValidateFilters(v, tt.filters)

			// Assert: Check that the errors in the Validator match the expected errors.
			if len(v.Errors) != len(tt.want) {
				t.Errorf("ValidateFilters() errors = %v, want %v", v.Errors, tt.want)
			}
			for key, msg := range tt.want {
				if v.Errors[key] != msg {
					t.Errorf("ValidateFilters() error for %s = %v, want %v", key, v.Errors[key], msg)
				}
			}
		})
	}
}
