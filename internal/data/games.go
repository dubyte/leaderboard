package data

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"
	"unicode/utf8"

	"github.com/lib/pq"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

type Game struct {
	ID        int       `json:"-"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"-"`
	Version   int       `json:"version"`
}

type GameModel struct {
	DB *sql.DB
}

const GameNameMaxLen = 12

// Create add the inserts a raw in games table
func (g GameModel) Create(game *Game) error {
	query := `
	INSERT INTO games (name)
	VALUES ($1)
	RETURNING ID, created_at, version`

	err := g.DB.QueryRow(query, game.Name).Scan(&game.ID, &game.CreatedAt, &game.Version) //nolint: execinquery
	if err != nil {
		var pqError *pq.Error
		switch {
		case errors.As(err, &pqError) && pqError.Code.Name() == "unique_violation":
			return ErrEditConflict
		default:
			return err
		}
	}

	return nil
}

// Read uses the passed id to find a game from games table.
func (g GameModel) Read(id int) (*Game, error) {
	if id < 1 {
		return nil, ErrRecordNotFound
	}

	query := `
	SELECT id, name, version, created_at
	FROM GAMES
	WHERE id = $1`

	var game Game

	err := g.DB.QueryRow(query, id).Scan(&game.ID, &game.Name, &game.Version, &game.CreatedAt)

	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, ErrRecordNotFound
		default:
			return nil, err
		}
	}

	return &game, nil
}

// Update use the passed game to update an existing game
func (g GameModel) Update(game *Game) error {
	query := `
	UPDATE games
	SET name = $1, version = version + 1
	WHERE id = $2
	RETURNING version`

	args := []any{game.Name, game.ID}

	err := g.DB.QueryRow(query, args...).Scan(&game.Version) //nolint: execinquery
	if err != nil {
		var pqError *pq.Error
		switch {
		case errors.As(err, &pqError):
			return ErrEditConflict
		default:
			return err
		}
	}

	return err
}

// Delete use the passed id to delete the row with that id on games table.
func (g GameModel) Delete(id int) error {
	if id < 1 {
		return ErrRecordNotFound
	}

	query := `
	DELETE FROM games
	WHERE id = $1`

	result, err := g.DB.Exec(query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrRecordNotFound
	}

	return nil
}

// List return a list of the games from the games table.
func (g GameModel) List(name string, filters Filters) ([]*Game, Metadata, error) {
	// Perform a paginated full-text search on the 'games' table, sorting by the specified column and direction.
	//nolint: gosec
	query := fmt.Sprintf(`
        SELECT count(*) OVER(), id, name, version, created_at
        FROM games
        WHERE (to_tsvector('simple', name) @@ plainto_tsquery('simple', $1) OR $1 = '')      
        ORDER BY %s %s, id ASC
        LIMIT $2 OFFSET $3`, filters.sortColumn(), filters.sortDirection())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	args := []any{name, filters.limit(), filters.offset()}

	rows, err := g.DB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, Metadata{}, err
	}
	defer rows.Close()

	var totalRecords int
	games := make([]*Game, 0)
	for rows.Next() {
		var game Game
		err = rows.Scan(&totalRecords, &game.ID, &game.Name, &game.Version, &game.CreatedAt)
		if err != nil {
			return nil, Metadata{}, err
		}

		games = append(games, &game)
	}

	// check if there was any error during iteration.
	if err = rows.Err(); err != nil {
		return nil, Metadata{}, err
	}

	metadata := calculateMetadata(totalRecords, filters.Page, filters.PageSize)

	return games, metadata, nil
}

// Validate game check the game is correct
func ValidateGame(v *validator.Validator, game *Game) {
	v.Check(game.Name != "", "name", "can not be empyt")
	v.Check(utf8.RuneCountInString(game.Name) <= GameNameMaxLen, "name", fmt.Sprintf("name should not exceed %d characters", GameNameMaxLen))
}
