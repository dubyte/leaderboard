package validator

import (
	"regexp"
	"slices"
	"strconv"
)

// Define a new validator type which contains a map of validation errors.
type Validator struct {
	Errors map[string]string
}

// New is a helper which creates a new Validator instance with an empty errors map.
func New() *Validator {
	return &Validator{Errors: make(map[string]string)}
}

// Valid returns true if the errors map doesn't contain any entries.
func (v *Validator) Valid() bool {
	return len(v.Errors) == 0
}

// AddError ads an error message to the map checking is not already present.
func (v *Validator) AddError(key, message string) {
	if _, ok := v.Errors[key]; !ok {
		v.Errors[key] = message
	}
}

// Check ads an error message to the map only if a validation check is not 'ok'.
func (v *Validator) Check(ok bool, key, message string) {
	if !ok {
		v.AddError(key, message)
	}
}

// Generic function which returns true if a specific value is in a list of permitted
// values
func PermittedValue[T comparable](value T, permittedValues ...T) bool {
	return slices.Contains(permittedValues, value)
}

// Matches returns true if a string value matches a specific regexp pattern
func Matches(value string, rx *regexp.Regexp) bool {
	return rx.MatchString(value)
}

// Generic function which returns true if all values in a slice are unique.
func Unique[T comparable](values []T) bool {
	// originaly was map[T]bool
	// using struct{} becasue use less memory than bool
	uniqueValues := make(map[T]struct{})

	for _, values := range values {
		uniqueValues[values] = struct{}{}
	}

	return len(values) == len(uniqueValues)
}

func Numeric(n string) bool {
	_, err := strconv.Atoi(n)
	return err == nil
}
