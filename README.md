
# Dubyte Leaderboard

[![pipeline status](https://gitlab.com/dubyte/leaderboard/badges/main/pipeline.svg)](https://gitlab.com/dubyte/leaderboard/-/commits/main)
[![coverage report](https://gitlab.com/dubyte/leaderboard/badges/main/coverage.svg)](https://gitlab.com/dubyte/leaderboard/-/commits/main)
[![Latest Release](https://gitlab.com/dubyte/leaderboard/-/badges/release.svg)](https://gitlab.com/dubyte/leaderboard/-/releases)

Dubyte Leaderboard aims to handle a simple way to record the scores of multiple games.

Dubyte Leaderboard aims:

- Record score for games
- Use an sql storage currently Postgres only
- It's a work in progress notice there is not version 0.0.1

## Environment variables

- LEADERBOARD_DB_DSN (used to tell the service how to connect to postgresql): i.e."postgres://leaderboardsrv:password@localhost/leaderboardsrv"

## Reference

- [Let's go](https://lets-go.alexedwards.net/)
- [Let's go further](https://lets-go-further.alexedwards.net/)
- [Mockery](https://vektra.github.io/mockery/latest/)
- [sqlmock](https://github.com/DATA-DOG/go-sqlmock)
- [migrate](https://github.com/golang-migrate/migrate)
