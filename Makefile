# Include variables from the .envrc file
include .envrc

## help: print this help message
.PHONY: help
help:
	@echo 'Usage:'
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'	

.PHONY: confirm
confirm:
	@echo -n 'Are you sure? [y/N] '&& read ans && [ $${ans:-N} = y ]

## run/api: run the cmd/api application
.PHONY: run/api
run/api:
	@go run ./cmd/api -db-dsn=${LEADERBOARD_DB_DSN}

## db/psql: connect to the database using psql
.PHONY: db/psql
db/psql:
	@psql ${LEADERBOARD_DB_DSN}

## db/migrations/new name=$1: create a new database migration
.PHONY: db/migration/new
db/migration/new:
	@echo 'Creating migration files for ${name}...'
	migrate create -seq -ext=.sql -dir=./migrations ${name}

## db/migrations/up: apply all up database migrations
.PHONY: db/migration/up
db/migration/up: confirm
	@echo 'Running up migrations...'
	@migrate -path ./migrations -database ${LEADERBOARD_DB_DSN} up

## db/migrations/drop: drop all database migrations
.PHONY: db/migration/drop
db/migration/drop:
	@echo 'Running drop migrations...'
	@migrate -path ./migrations -database ${LEADERBOARD_DB_DSN} drop

## gen/mockery: generate mocks using mockery
.PHONY: gen/mockery
gen/mockery:
	mockery

## run/build: build the application
.PHONY: build/api
build/api:
	@echo 'Building cmd/api...'
	go build -a -ldflags='-s' -o=./bin/api  ./cmd/api 

## audit: tidy dependencies and format, vet and test all code
.PHONY: audit
audit:
	@echo 'Tidying and verifying module dependencies...'
	go mod tidy
	go mod verify
	@echo 'Formatting code...'
	go fmt ./...
	@echo 'Linting code...'
	go vet ./...
	golangci-lint run --tests=false --presets=bugs,sql,performance #format,error,style,complexity,comment
	@echo 'Running tests...'
	go test -race -vet=off ./...
