package main

import (
	"bytes"
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/dubyte/leaderboard/internal/data"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func TestSetGame(t *testing.T) {
	noopValidate := func(v *validator.Validator, game *data.Game) {}
	var buildApp = func(
		m data.GamesCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game)) application {
		return application{
			config:           config{},
			logger:           slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:           data.Models{Games: m},
			validateGameFunc: validateGameFunc}
	}

	t.Run("Happy Path", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Create(mock.Anything).Return(nil)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games", strings.NewReader(`{"name": "hello"}`))

		// Act
		app.setGame(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusCreated, res.StatusCode)
		assert.JSONEq(t, `{"game":{"name":"hello","version":0}}`, string(data))
	})

	t.Run("bad json return badRequest", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games", strings.NewReader(`{"}`))

		// Act
		app.setGame(w, r)

		// Assert
		w.Flush()
		data, err := io.ReadAll(w.Result().Body)
		defer w.Result().Body.Close()
		require.NoError(t, err)
		require.Equal(t, http.StatusBadRequest, w.Result().StatusCode)
		assert.JSONEq(t, `{"error":"body contains badly-formed JSON"}`, string(data))
	})

	t.Run("validate errors return StatusUnprocessableEntity", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		// validator insert errors
		app := buildApp(m, func(v *validator.Validator, _ *data.Game) { v.AddError("something", assert.AnError.Error()) })
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games", strings.NewReader(`{"name":""}`))

		// Act
		app.setGame(w, r)

		// Assert
		w.Flush()
		resultBody, err := io.ReadAll(w.Result().Body)
		defer w.Result().Body.Close()
		require.NoError(t, err)
		require.Equal(t, http.StatusUnprocessableEntity, w.Result().StatusCode)
		assert.JSONEq(t, `{"error":{"something":"assert.AnError general error for testing"}}`, string(resultBody))
	})

	t.Run("create returns duplicate error -> StatusConflict", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Create(mock.Anything).Return(data.ErrEditConflict)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games", strings.NewReader(`{"name":"AAA's game"}`))

		// Act
		app.setGame(w, r)

		// Assert
		w.Flush()
		assertEditConflict(t, w.Result())
	})

	t.Run("create returns another error -> StatusInternalSeverError", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Create(mock.Anything).Return(assert.AnError)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games", strings.NewReader(`{"name":"AAA's game"}`))

		// Act
		app.setGame(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}

func TestGetGame(t *testing.T) {
	noopValidate := func(v *validator.Validator, game *data.Game) {}
	var buildApp = func(
		m data.GamesCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game)) application {
		return application{
			config:           config{},
			logger:           slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:           data.Models{Games: m},
			validateGameFunc: validateGameFunc}
	}

	t.Run("Happy Path", func(t *testing.T) {
		// Setup
		now := time.Now()
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: now}, nil)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1", nil)
		r.SetPathValue("gameID", "1")

		// Act
		app.getGame(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		resultBody, err := io.ReadAll(w.Result().Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusCreated, w.Result().StatusCode)
		assert.JSONEq(t, `{"game":{"name":"testgame","version":1}}`, string(resultBody))
	})

	t.Run("gameID is not numeric -> return not found", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		// keep this for consistent but the actual setter is setPathValue
		r := httptest.NewRequest("GET", "/v1/games/testgame", nil)
		r.SetPathValue("gameID", "testgame")

		// Act
		app.getGame(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("game is not found in data store -> return http status not found", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1", nil)
		r.SetPathValue("gameID", "1")

		// Act
		app.getGame(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("dataStore.Read returns an un expected error -> return http internal server error", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1", nil)
		r.SetPathValue("gameID", "1")

		// Act
		app.getGame(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}

func TestDeleteGame(t *testing.T) {
	noopValidate := func(v *validator.Validator, game *data.Game) {}
	var buildApp = func(
		m data.GamesCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game)) application {
		return application{
			config:           config{},
			logger:           slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:           data.Models{Games: m},
			validateGameFunc: validateGameFunc}
	}

	t.Run("Happy Path", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Delete(mock.Anything).Return(nil)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1", strings.NewReader(`{"name":"AAA game"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.deleteGame(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"message":"game successfully deleted"}`, string(data))
	})

	t.Run("no numeric gameID -> returning not found", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		// setting /v1/games/testgame for consistency but on this test the setPathValue is the effective way to set the value.
		r := httptest.NewRequest("PUT", "/v1/games/testgame", strings.NewReader(`{"name":"AAA game"}`))
		r.SetPathValue("gameID", "testgame")

		// Act
		app.deleteGame(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("data storage return not found", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Delete(mock.Anything).Return(data.ErrRecordNotFound)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1", strings.NewReader(`{"name":"AAA game"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.deleteGame(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusNotFound, res.StatusCode)
		assert.JSONEq(t, `{"error":"the requested resource could not be found"}`, string(data))
	})

	t.Run("data storage return unexpected error -> rerturns internal server error", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().Delete(mock.Anything).Return(assert.AnError)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1", strings.NewReader(`{"name":"AAA game"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.deleteGame(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}

func TestListGames(t *testing.T) {
	noopValidate := func(v *validator.Validator, game *data.Game) {}
	buildApp := func(
		m data.GamesCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game)) application {
		return application{
			config:           config{},
			logger:           slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:           data.Models{Games: m},
			validateGameFunc: validateGameFunc}
	}

	t.Run("Happy Path", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		game := &data.Game{ID: 1, Name: "testgame", CreatedAt: time.Now(), Version: 1}
		m.EXPECT().List("", mock.Anything).Return([]*data.Game{game}, data.Metadata{}, nil)

		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games", nil)
		params := url.Values{}
		params.Add("name", "")
		params.Add("page", "1")
		params.Add("page_size", "10")
		params.Add("sort", "id")
		r.URL.RawQuery = params.Encode()

		// Act
		app.listGames(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"games":[{"name":"testgame", "version":1 }], "metadata":{}}`, string(data))
	})

	t.Run("Happy Path empty list", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().List("", mock.Anything).Return(make([]*data.Game, 0), data.Metadata{}, nil)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games", nil)

		// Act
		app.listGames(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"games":[],"metadata":{}}`, string(data))
		// I want to be sure that the return value has literally []
		assert.Equal(t, `{"games":[],"metadata":{}}`, string(bytes.TrimSpace(data)))
	})

	t.Run("Filters' validation fails", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games", nil)
		params := url.Values{}
		params.Add("name", "")
		params.Add("page", "-1")
		params.Add("page_size", "10")
		params.Add("sort", "id")
		r.URL.RawQuery = params.Encode()

		// Act
		app.listGames(w, r)

		// Assert
		w.Flush()
		assertUnprocessableEntity(t, w.Result(), `{"error":{"page":"must be greater than zero"}}`)
	})

	t.Run("data store return unexpected error -> return internal server error", func(t *testing.T) {
		// Setup
		m := data.NewMockGamesCRUDL(t)
		m.EXPECT().List("", mock.Anything).Return(nil, data.Metadata{}, assert.AnError)
		app := buildApp(m, noopValidate)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games", nil)

		// Act
		app.listGames(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}
