package main

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/dubyte/leaderboard/internal/data"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func (app *application) setGame(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Name string `json:"name"`
	}

	err := app.readJSON(w, r, &input)
	if err != nil {
		app.badRequestResponse(w, r, err)
		return
	}

	game := &data.Game{
		Name: input.Name,
	}

	v := validator.New()
	app.validateGameFunc(v, game)
	if !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	if err = app.models.Games.Create(game); err != nil {
		switch {
		case errors.Is(err, data.ErrEditConflict):
			app.conflict(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}

		return
	}

	headers := make(http.Header)
	headers.Set("Location", fmt.Sprintf("/v1/games/%d", game.ID))

	if err = app.writeJSON(w, http.StatusCreated, envelope{"game": game}, headers); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) getGame(w http.ResponseWriter, r *http.Request) {
	id, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	game, err := app.models.Games.Read(id)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	if err = app.writeJSON(w, http.StatusCreated, envelope{"game": game}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) deleteGame(w http.ResponseWriter, r *http.Request) {
	id, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	if err := app.models.Games.Delete(id); err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	err = app.writeJSON(w, http.StatusOK, envelope{"message": "game successfully deleted"}, nil)
	if err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) listGames(w http.ResponseWriter, r *http.Request) {
	var input struct {
		Name string
		data.Filters
	}

	v := validator.New()

	qs := r.URL.Query()

	input.Name = app.readString(qs, "name", "")
	input.Filters.Page = app.readInt(qs, "page", 1, v)
	input.Filters.PageSize = app.readInt(qs, "page_size", 20, v)
	input.Filters.Sort = app.readString(qs, "sort", "id")
	input.Filters.SortSafelist = []string{"id", "name", "-id", "-name"}

	if data.ValidateFilters(v, input.Filters); !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	games, metadata, err := app.models.Games.List(input.Name, input.Filters)
	if err != nil {
		app.serverErrorResponse(w, r, err)
		return
	}

	if err = app.writeJSON(w, http.StatusOK, envelope{"games": games, "metadata": metadata}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}
