package main

import (
	"io"
	"log/slog"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHealthcheckHandler(t *testing.T) {
	t.Run("happy path", func(t *testing.T) {
		// Setup
		app := application{config: config{}, logger: slog.New(slog.NewTextHandler(io.Discard, nil))}
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/healthcheck", nil)

		// Act
		app.healthcheckHandler(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		body, err := io.ReadAll(w.Result().Body)
		require.NoError(t, err)
		assert.JSONEq(t, `{"status":"available", "system_info": {"environment":"", "version":""}}`, string(body))
	})
}
