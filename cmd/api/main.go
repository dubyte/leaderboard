package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/dubyte/leaderboard/internal/data"
	"gitlab.com/dubyte/leaderboard/internal/validator"
	"gitlab.com/dubyte/leaderboard/internal/vcs"
)

var (
	version = vcs.Version()
)

type config struct {
	port int
	env  string
	db   struct {
		dsn          string
		maxOpenConns int
		maxIdleConns int
		maxIdleTime  time.Duration
	}
}

type application struct {
	config                  config
	logger                  *slog.Logger
	models                  data.Models
	validateGameFunc        func(v *validator.Validator, game *data.Game)
	validateLeaderboardFunc func(v *validator.Validator, leaderboard *data.Leaderboard)
	validateScoreFunc       func(v *validator.Validator, leaderboard *data.Score)
}

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer cancel()

	// Declaring an instance of the config struct
	var cfg config

	// Read the value of the port and env command line flags into config struct.
	flag.IntVar(&cfg.port, "port", 8080, "HTTP Server port")
	flag.StringVar(&cfg.env, "env", "development", "Environment (development|staging|production)")

	flag.StringVar(&cfg.db.dsn, "db-dsn", "", "PostgreSQL DSN")
	flag.IntVar(&cfg.db.maxOpenConns, "db-max-open-conns", 25, "PostgreSQL max open connections")
	flag.IntVar(&cfg.db.maxIdleConns, "db-max-idle-conns", 25, "PostgreSQL max idle connections")
	flag.DurationVar(&cfg.db.maxIdleTime, "db-max-idle-time", 15*time.Minute, "PostgreSQL max connection idle time")

	displayVersion := flag.Bool("version", false, "Display version and exit")
	flag.Parse()

	if *displayVersion {
		fmt.Printf("Version:\t%s\n", version)
		os.Exit(0)
	}

	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))

	db, err := openDB(cfg)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
	defer db.Close()

	logger.Info("database connection pool established")

	// Declare instance of the application struct
	app := &application{
		config:                  cfg,
		logger:                  logger,
		models:                  data.NewModels(db),
		validateGameFunc:        data.ValidateGame,
		validateLeaderboardFunc: data.ValidateLeaderboard,
		validateScoreFunc:       data.ValidateScore,
	}

	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", cfg.port),
		Handler:      app.routes(),
		BaseContext:  func(l net.Listener) context.Context { return ctx },
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		ErrorLog:     slog.NewLogLogger(logger.Handler(), slog.LevelError),
	}

	// Start the http server
	logger.Info("starting server", "addr", srv.Addr, "env", cfg.env)

	// Application will start to listen and serve this function will locking main thread
	srvErr := make(chan error, 1)
	go func() {
		srvErr <- srv.ListenAndServe()
	}()

	select {
	case err = <-srvErr:
		logger.Error(err.Error())
		os.Exit(1)
	case <-ctx.Done():
		// Wait for first CTRL+C.
		// Stop receiving signal notifications as soon as possible.
		cancel()
	}
}

// openDB create a db connection pool and set the max open/idle connections and max idle time
func openDB(cfg config) (*sql.DB, error) {
	db, err := sql.Open("postgres", cfg.db.dsn)
	if err != nil {
		return nil, err
	}

	// SetMaxOpenConns sets the maximum number of open connections to the database.
	db.SetMaxOpenConns(cfg.db.maxOpenConns)

	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	db.SetMaxIdleConns(cfg.db.maxIdleConns)

	// SetConnMaxIdleTime sets the maximum amount of time a connection may be idle.
	db.SetConnMaxIdleTime(cfg.db.maxIdleTime)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	// Confirms the connection is ok to continue.
	err = db.PingContext(ctx)
	if err != nil {
		db.Close()
		return nil, err
	}

	return db, nil
}
