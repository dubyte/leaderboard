package main

import (
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/dubyte/leaderboard/internal/data"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func TestSetScore(t *testing.T) {
	noopValidateScore := func(v *validator.Validator, game *data.Score) {}

	var buildApp = func(
		mLeaderboard data.LeaderboardsCRUDL,
		mScore data.ScoresCRUDL,
		validateScoreFunc func(_ *validator.Validator, _ *data.Score)) application {
		return application{
			config:            config{},
			logger:            slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:            data.Models{Scores: mScore, Leaderboards: mLeaderboard},
			validateScoreFunc: validateScoreFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Create(mock.Anything).Return(nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusCreated, res.StatusCode)
		assert.JSONEq(t, `{"score":{"leaderboard_id":1,"username":"test", "value":"100","version":0}}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "anumber")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/0/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "0")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("POST", "/v1/games/-10/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "-10")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/anumber/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "anumber")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/0/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "0")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/-10/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "-10")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})
	t.Run("leaderboard not found", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read game from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("gameId from leaderboard read call not match with endpoint one.", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 100, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("Leaderboard validation has errors", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, func(v *validator.Validator, _ *data.Score) { v.AddError("something", "something") })
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusUnprocessableEntity, res.StatusCode)
		assert.JSONEq(t, `{"error":{"something":"something"}}`, string(data))
	})

	t.Run("try to create a leaderboard with a name already taken", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Create(mock.Anything).Return(data.ErrEditConflict)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertEditConflict(t, w.Result())
	})

	t.Run("storage create return un expected error", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Create(mock.Anything).Return(assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards/1/scores", strings.NewReader(`{"username": "test", "value": "100"}`))
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.setScore(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}

func TestGeScore(t *testing.T) {
	noopValidateScore := func(v *validator.Validator, game *data.Score) {}
	var buildApp = func(
		mLeaderboard data.LeaderboardsCRUDL,
		mScore data.ScoresCRUDL,
		validateScoreFunc func(_ *validator.Validator, _ *data.Score)) application {
		return application{
			config:            config{},
			logger:            slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:            data.Models{Scores: mScore, Leaderboards: mLeaderboard},
			validateScoreFunc: validateScoreFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Read(mock.Anything).Return(&data.Score{ID: 1, Username: "test", LeaderboardID: 1, Value: "100", Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"score":{"leaderboard_id":1,"username":"test", "value":"100","version":1}}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/anumber/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "anumber")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/0/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "0")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/-10/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "-10")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/anumber/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "anumber")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/0/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "0")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/-10/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "-10")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboard not found", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read leaderboard from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("leaderboard's gameID does not match with endpoint one", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 100, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("scoreID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/anumber", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "anumber")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("scoreID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/0", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "0")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("scoreID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/-10", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "-10")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("score not found", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read score from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("leaderboardID from endpoint does not match with leaderboardID on Score", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Read(mock.Anything).Return(&data.Score{ID: 1, Username: "test", LeaderboardID: 100, Value: "100", Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.getScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})
}

func TestDeleteScore(t *testing.T) {
	noopValidateScore := func(v *validator.Validator, game *data.Score) {}
	var buildApp = func(
		mLeaderboard data.LeaderboardsCRUDL,
		mScore data.ScoresCRUDL,
		validateScoreFunc func(_ *validator.Validator, _ *data.Score)) application {
		return application{
			config:            config{},
			logger:            slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:            data.Models{Scores: mScore, Leaderboards: mLeaderboard},
			validateScoreFunc: validateScoreFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Delete(mock.Anything).Return(nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"message": "score successfully deleted"}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/anumber/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "anumber")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/0/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "0")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/-10/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "-10")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/anumber/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "anumber")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/0/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "0")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/-10/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "-10")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboard not found", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read leaderboard from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("leaderboard's gameID does not match with endpoint one", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 100, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("scoreID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/anumber", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "anumber")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("scoreID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/0", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "0")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("scoreID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/-10", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "-10")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("score to delete not found", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Delete(mock.Anything).Return(data.ErrRecordNotFound)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("delete score from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().Delete(mock.Anything).Return(assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		r.SetPathValue("scoreID", "1")

		// Act
		app.deleteScore(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}

func TestListScore(t *testing.T) {
	noopValidateScore := func(v *validator.Validator, game *data.Score) {}

	var buildApp = func(
		mLeaderboard data.LeaderboardsCRUDL,
		mScore data.ScoresCRUDL,
		validateScoreFunc func(_ *validator.Validator, _ *data.Score)) application {
		return application{
			config:            config{},
			logger:            slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:            data.Models{Scores: mScore, Leaderboards: mLeaderboard},
			validateScoreFunc: validateScoreFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().List(1, mock.Anything).
			Return([]*data.Score{{ID: 1, LeaderboardID: 1, Username: "test", Value: "100", Version: 1, CreatedAt: time.Now()}}, data.Metadata{}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		params := url.Values{}
		params.Add("page", "1")
		params.Add("page_size", "10")
		params.Add("sort", "id")
		r.URL.RawQuery = params.Encode()

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"scores":[{"leaderboard_id":1,"username":"test", "value":"100","version":1}],"metadata":{}}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "anumber")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/0/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "0")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/-10/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "-10")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is not numeric", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/anumber/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "anumber")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is zero", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/0/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "0")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is negative", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/-10/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "-10")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboard not found", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read game from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("gameId from leaderboard read call not match with endpoint one.", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 100, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("Filter verification fails", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")
		params := url.Values{}
		params.Add("page", "-1")
		params.Add("page_size", "10")
		params.Add("sort", "id")
		r.URL.RawQuery = params.Encode()

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertUnprocessableEntity(t, w.Result(), `{"error":{"page":"must be greater than zero"}}`)
	})

	t.Run("storage list return un expected error", func(t *testing.T) {
		// Setup
		mockL, mockS := data.NewMockLeaderboardsCRUDL(t), data.NewMockScoresCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockS.EXPECT().List(1, mock.Anything).Return(nil, data.Metadata{}, assert.AnError)
		app := buildApp(mockL, mockS, noopValidateScore)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1/scores", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.listScores(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}
