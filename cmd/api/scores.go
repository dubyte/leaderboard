package main

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/dubyte/leaderboard/internal/data"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func (app *application) setScore(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboardID, err := app.readIDParam("leaderboardID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboard, err := app.models.Leaderboards.Read(leaderboardID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	// The leaderboard gameID must match the gameID in the endpoint
	if leaderboard.GameID != gameID {
		app.notFoundResponse(w, r)
		return
	}

	var input struct {
		Username string `json:"username"`
		Value    string `json:"value"`
	}

	err = app.readJSON(w, r, &input)
	if err != nil {
		app.badRequestResponse(w, r, err)
		return
	}

	score := &data.Score{
		Username:      input.Username,
		LeaderboardID: leaderboardID,
		Value:         input.Value,
	}

	v := validator.New()
	app.validateScoreFunc(v, score)
	if !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	if err = app.models.Scores.Create(score); err != nil {
		switch {
		case errors.Is(err, data.ErrEditConflict):
			app.conflict(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	headers := make(http.Header)
	headers.Set("Location", fmt.Sprintf("/v1/games/%d/leaderboards/%d/scores/%d", gameID, leaderboardID, score.ID))

	if err = app.writeJSON(w, http.StatusCreated, envelope{"score": score}, headers); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) getScore(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboardID, err := app.readIDParam("leaderboardID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboard, err := app.models.Leaderboards.Read(leaderboardID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	// if leaderboard's gameID not match with endpoint one return not found
	if leaderboard.GameID != gameID {
		app.notFoundResponse(w, r)
		return
	}

	ID, err := app.readIDParam("scoreID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	score, err := app.models.Scores.Read(ID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	// if the score found by id dont belong to the leaderboard return not found.
	if score.LeaderboardID != leaderboardID {
		app.notFoundResponse(w, r)
		return
	}

	if err = app.writeJSON(w, http.StatusOK, envelope{"score": score}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) deleteScore(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboardID, err := app.readIDParam("leaderboardID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboard, err := app.models.Leaderboards.Read(leaderboardID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	// if leaderboard's gameID not match with endpoint one return not found
	if leaderboard.GameID != gameID {
		app.notFoundResponse(w, r)
		return
	}

	ID, err := app.readIDParam("scoreID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	err = app.models.Scores.Delete(ID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	if err = app.writeJSON(w, http.StatusOK, envelope{"message": "score successfully deleted"}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) listScores(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboardID, err := app.readIDParam("leaderboardID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboard, err := app.models.Leaderboards.Read(leaderboardID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	// if leaderboard's gameID not match with endpoint one return not found
	if leaderboard.GameID != gameID {
		app.notFoundResponse(w, r)
		return
	}

	// we could use directly data.Filters but using input for consistency
	var input struct {
		data.Filters
	}

	v := validator.New()

	qs := r.URL.Query()

	input.Filters.Page = app.readInt(qs, "page", 1, v)
	input.Filters.PageSize = app.readInt(qs, "page_size", 20, v)
	input.Filters.Sort = app.readString(qs, "sort", "id")
	input.Filters.SortSafelist = []string{"id", "value", "-id", "-value"}

	if data.ValidateFilters(v, input.Filters); !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	scores, metadata, err := app.models.Scores.List(leaderboardID, input.Filters)
	if err != nil {
		app.serverErrorResponse(w, r, err)
		return
	}

	if err = app.writeJSON(w, http.StatusOK, envelope{"scores": scores, "metadata": metadata}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}
