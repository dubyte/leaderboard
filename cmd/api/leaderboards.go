package main

import (
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/dubyte/leaderboard/internal/data"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func (app *application) setLeaderboard(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	game, err := app.models.Games.Read(gameID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	var input struct {
		Name string `json:"name"`
	}

	err = app.readJSON(w, r, &input)
	if err != nil {
		app.badRequestResponse(w, r, err)
		return
	}

	leaderboard := &data.Leaderboard{
		Name:   input.Name,
		GameID: game.ID,
	}

	v := validator.New()
	app.validateLeaderboardFunc(v, leaderboard)
	if !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	if err = app.models.Leaderboards.Create(leaderboard); err != nil {
		switch {
		case errors.Is(err, data.ErrEditConflict):
			app.conflict(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	headers := make(http.Header)
	headers.Set("Location", fmt.Sprintf("/v1/games/%d/leaderboards/%d", gameID, leaderboard.ID))

	if err = app.writeJSON(w, http.StatusCreated, envelope{"leaderboard": leaderboard}, headers); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) getLeaderboard(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	game, err := app.models.Games.Read(gameID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	ID, err := app.readIDParam("leaderboardID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	leaderboard, err := app.models.Leaderboards.Read(ID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	// if the leaderboard found by id dont belong to the game id return not found.
	if leaderboard.GameID != game.ID {
		app.notFoundResponse(w, r)
		return
	}

	if err = app.writeJSON(w, http.StatusOK, envelope{"leaderboard": leaderboard}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) deleteLeaderboard(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	ID, err := app.readIDParam("leaderboardID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	l, err := app.models.Leaderboards.Read(ID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	// if game from the returned leaderboard dont match with gameID from endpoint
	// return not found (a leaderboard id that belongs with that gameID)
	if l.GameID != gameID {
		app.notFoundResponse(w, r)
		return
	}

	err = app.models.Leaderboards.Delete(ID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	if err = app.writeJSON(w, http.StatusOK, envelope{"message": "leaderboard successfully deleted"}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}

func (app *application) listLeaderboards(w http.ResponseWriter, r *http.Request) {
	gameID, err := app.readIDParam("gameID", r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	_, err = app.models.Games.Read(gameID)
	if err != nil {
		switch {
		case errors.Is(err, data.ErrRecordNotFound):
			app.notFoundResponse(w, r)
		default:
			app.serverErrorResponse(w, r, err)
		}
		return
	}

	var input struct {
		Name string
		data.Filters
	}

	v := validator.New()

	qs := r.URL.Query()

	input.Name = app.readString(qs, "name", "")
	input.Filters.Page = app.readInt(qs, "page", 1, v)
	input.Filters.PageSize = app.readInt(qs, "page_size", 20, v)
	input.Filters.Sort = app.readString(qs, "sort", "id")
	input.Filters.SortSafelist = []string{"id", "name", "-id", "-name"}

	if data.ValidateFilters(v, input.Filters); !v.Valid() {
		app.failedValidationResponse(w, r, v.Errors)
		return
	}

	leaderboards, metadata, err := app.models.Leaderboards.List(input.Name, gameID, input.Filters)
	if err != nil {
		app.serverErrorResponse(w, r, err)
		return
	}

	if err = app.writeJSON(w, http.StatusOK, envelope{"leaderboards": leaderboards, "metadata": metadata}, nil); err != nil {
		app.serverErrorResponse(w, r, err)
	}
}
