package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func (app *application) readIDParam(name string, r *http.Request) (int, error) {
	id, err := strconv.ParseInt(r.PathValue(name), 10, 64)
	if err != nil || id < 1 {
		return 0, fmt.Errorf("invalid %s parameter", name)
	}

	return int(id), nil
}

type envelope map[string]any

func (app *application) writeJSON(w http.ResponseWriter, status int, data any, headers http.Header) error {
	js, err := json.Marshal(data)
	if err != nil {
		return err
	}

	// Append a new line to json to make it easier to view on terminal applications
	js = append(js, '\n')

	for key, value := range headers {
		w.Header()[key] = value
	}

	// write JSON header then write status
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	// Write JSOJN to the HTTP response body
	_, err = w.Write(js)
	if err != nil {
		return err
	}

	return nil
}

func (app *application) logError(r *http.Request, err error) {
	var (
		method = r.Method
		uri    = r.URL.RequestURI()
	)

	app.logger.Error(err.Error(), "method", method, "uri", uri)
}

func (app *application) errorResponse(w http.ResponseWriter, r *http.Request, status int, message any) {
	env := envelope{"error": message}

	err := app.writeJSON(w, status, env, nil)
	if err != nil {
		app.logError(r, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func (app *application) serverErrorResponse(w http.ResponseWriter, r *http.Request, err error) {
	app.logError(r, err)

	message := "the server encountered a problem and could not process your request"
	app.errorResponse(w, r, http.StatusInternalServerError, message)
}

func (app *application) notFoundResponse(w http.ResponseWriter, r *http.Request) {
	message := "the requested resource could not be found"
	app.errorResponse(w, r, http.StatusNotFound, message)
}

func (app *application) conflict(w http.ResponseWriter, r *http.Request) {
	message := "conflict with existing record"
	app.errorResponse(w, r, http.StatusConflict, message)
}

func (app *application) readJSON(w http.ResponseWriter, r *http.Request, dst any) error {
	// Use http.MaxBytesReader to limit the size of the request body to 1MB
	maxBytes := 1_048_576
	r.Body = http.MaxBytesReader(w, r.Body, int64(maxBytes))

	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	// Decode request body
	err := dec.Decode(dst)
	if err != nil {
		// If there is an error during decoding, start the triage...
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		var invalidUnmarshalError *json.InvalidUnmarshalError
		var maxBytesError *http.MaxBytesError
		switch {
		case errors.As(err, &syntaxError):
			return fmt.Errorf("body contains badly-formed JSON (at character %d)", syntaxError.Offset)

		// In some circumstances Decode() may also return an io.ErrUnexpectedEOF error
		// for syntax errors in the JSON. So we check for this using errors.Is() and
		// return a generic error message. There is an open issue regarding this at
		// https://github.com/golang/go/issues/25956.
		case errors.Is(err, io.ErrUnexpectedEOF):
			return errors.New("body contains badly-formed JSON")

		case errors.As(err, &unmarshalTypeError):
			if unmarshalTypeError.Field != "" {
				return fmt.Errorf("body contains incorrect JSON type for field %q", unmarshalTypeError.Field)
			}
			return fmt.Errorf("body contains incorrect JSON type (at character %d)", unmarshalTypeError.Offset)

			// An io.EOF error will be returned by Decode() if the request body is empty.
		case errors.Is(err, io.EOF):
			return errors.New("body must not be empty")

		//requeres: DisallowUnknownFields
		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			return fmt.Errorf("body contains unknown key %s", fieldName)

		case errors.As(err, &maxBytesError):
			return fmt.Errorf("body must not be larger than %d bytes", maxBytesError.Limit)

		case errors.As(err, &invalidUnmarshalError):
			panic(err)

		default:
			return err
		}
	}

	if dec.More() {
		return errors.New("body must only contain a single JSON value")
	}

	return nil
}

func (app *application) readString(qs url.Values, key string, defaultValue string) string {
	s := qs.Get(key)

	if s == "" {
		return defaultValue
	}

	return s
}

func (app *application) readInt(qs url.Values, key string, defaultValue int, v *validator.Validator) int {
	s := qs.Get(key)

	if s == "" {
		return defaultValue
	}

	i, err := strconv.Atoi(s)
	if err != nil {
		v.AddError(key, "must be an integer value")
		return defaultValue
	}

	return i
}

func (app *application) badRequestResponse(w http.ResponseWriter, r *http.Request, err error) {
	app.errorResponse(w, r, http.StatusBadRequest, err.Error())
}

func (app *application) failedValidationResponse(w http.ResponseWriter, r *http.Request, errors map[string]string) {
	app.errorResponse(w, r, http.StatusUnprocessableEntity, errors)
}

func assertInternalServerError(t *testing.T, r *http.Response) {
	t.Helper()
	defer r.Body.Close()
	data, err := io.ReadAll(r.Body)
	require.NoError(t, err)
	require.Equal(t, http.StatusInternalServerError, r.StatusCode)
	assert.JSONEq(t, `{"error":"the server encountered a problem and could not process your request"}`, string(data))
}

func assertNotFound(t *testing.T, r *http.Response) {
	t.Helper()
	defer r.Body.Close()
	data, err := io.ReadAll(r.Body)
	require.NoError(t, err)
	require.Equal(t, http.StatusNotFound, r.StatusCode)
	assert.JSONEq(t, `{"error":"the requested resource could not be found"}`, string(data))
}

func assertEditConflict(t *testing.T, r *http.Response) {
	t.Helper()
	defer r.Body.Close()
	data, err := io.ReadAll(r.Body)
	require.NoError(t, err)
	require.Equal(t, http.StatusConflict, r.StatusCode)
	assert.JSONEq(t, `{"error":"conflict with existing record"}`, string(data))
}

func assertUnprocessableEntity(t *testing.T, r *http.Response, expectedJsonResponse string) {
	t.Helper()
	defer r.Body.Close()
	data, err := io.ReadAll(r.Body)
	require.NoError(t, err)
	require.Equal(t, http.StatusUnprocessableEntity, r.StatusCode)
	assert.JSONEq(t, expectedJsonResponse, string(data))
}
