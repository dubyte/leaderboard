package main

import (
	"fmt"
	"net/http"
)

// recoverPanic handle panic to response with json serverError but if the panic happen after writting status and message it does not work
// TODO: "fix" that case?
func (app *application) recoverPanic(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// create deferred function (wich will always be run in the event of a panic in go)
		defer func() {
			// using built in recover function to know if there was a panic or not.
			if err := recover(); err != nil {
				//if there was a panic, set connection header to close
				// this act as a trigger so the http server will close the connection
				w.Header().Set("Connection", "close")
				// the error return by panic is any so we use fmt.Errorf to normalized
				// and use a helper function
				app.serverErrorResponse(w, r, fmt.Errorf("%s", err))
			}
		}()
		next.ServeHTTP(w, r)
	})
}
