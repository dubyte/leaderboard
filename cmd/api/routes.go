package main

import "net/http"

// routes return a handler with the api setup with the handlers
func (app *application) routes() http.Handler {
	mux := http.NewServeMux()

	// This route is used to override response from router when the handler is not setup.
	// return not found in json format
	mux.HandleFunc("/", app.notFoundResponse)

	// This route could be used by a cloud provider to know if the app is ready
	mux.HandleFunc("GET /v1/healthcheck", app.healthcheckHandler)

	// Game endpoints
	mux.HandleFunc("POST /v1/games", app.setGame)
	mux.HandleFunc("GET /v1/games/{gameID}", app.getGame)
	mux.HandleFunc("DELETE /v1/games/{gameID}", app.deleteGame)
	mux.HandleFunc("GET /v1/games", app.listGames)

	// Leaderboard endpoints
	mux.HandleFunc("POST /v1/games/{gameID}/leaderboards", app.setLeaderboard)
	mux.HandleFunc("GET /v1/games/{gameID}/leaderboards", app.listLeaderboards)
	mux.HandleFunc("GET /v1/games/{gameID}/leaderboards/{leaderboardID}", app.getLeaderboard)
	mux.HandleFunc("DELETE /v1/games/{gameID}/leaderboards/{leaderboardID}", app.deleteLeaderboard)

	// Score endpoints
	mux.HandleFunc("POST /v1/games/{gameID}/leaderboards/{leaderboardID}/scores", app.setScore)
	mux.HandleFunc("GET /v1/games/{gameID}/leaderboards/{leaderboardID}/scores/{scoreID}", app.getScore)
	mux.HandleFunc("DELETE /v1/games/{gameID}/leaderboards/{leaderboardID}/scores/{scoreID}", app.deleteScore)
	mux.HandleFunc("GET /v1/games/{gameID}/leaderboards/{leaderboardID}/scores", app.listScores)

	// wrapping mux to return a err response to user in case of panic in json format
	return app.recoverPanic(mux)
}
