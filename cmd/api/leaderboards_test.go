package main

import (
	"io"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/dubyte/leaderboard/internal/data"
	"gitlab.com/dubyte/leaderboard/internal/validator"
)

func TestSetLeaderboard(t *testing.T) {
	noopValidateGame := func(v *validator.Validator, game *data.Game) {}
	noopValidateLeaderboard := func(v *validator.Validator, game *data.Leaderboard) {}

	var buildApp = func(
		mGame data.GamesCRUDL,
		mLeaderboard data.LeaderboardsCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game),
		validateLeaderboardFunc func(_ *validator.Validator, _ *data.Leaderboard)) application {
		return application{
			config:                  config{},
			logger:                  slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:                  data.Models{Games: mGame, Leaderboards: mLeaderboard},
			validateGameFunc:        validateGameFunc,
			validateLeaderboardFunc: validateLeaderboardFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Create(mock.Anything).Return(nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusCreated, res.StatusCode)
		assert.JSONEq(t, `{"leaderboard":{"game_id":1,"name":"hiscore","version":0}}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "a number")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/0/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "0")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("POST", "/v1/games/-10/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "-10")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("game not found", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		// mockL.EXPECT().Create(mock.Anything).Return(nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read game from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		// mockL.EXPECT().Create(mock.Anything).Return(nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("Leaderboard validation has errors", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		//mockL.EXPECT().Create(mock.Anything).Return(nil)
		app := buildApp(mockG, mockL, noopValidateGame, func(v *validator.Validator, _ *data.Leaderboard) { v.AddError("something", "something") })
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusUnprocessableEntity, res.StatusCode)
		assert.JSONEq(t, `{"error":{"something":"something"}}`, string(data))
	})

	t.Run("try to create a leaderboard with a name already taken", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Create(mock.Anything).Return(data.ErrEditConflict)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		assertEditConflict(t, w.Result())
	})

	t.Run("storage create return un expected error", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Create(mock.Anything).Return(assert.AnError)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("POST", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.setLeaderboard(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}

func TestGetLeaderboard(t *testing.T) {
	noopValidateGame := func(v *validator.Validator, game *data.Game) {}
	noopValidateLeaderboard := func(v *validator.Validator, game *data.Leaderboard) {}

	var buildApp = func(
		mGame data.GamesCRUDL,
		mLeaderboard data.LeaderboardsCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game),
		validateLeaderboardFunc func(_ *validator.Validator, _ *data.Leaderboard)) application {
		return application{
			config:                  config{},
			logger:                  slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:                  data.Models{Games: mGame, Leaderboards: mLeaderboard},
			validateGameFunc:        validateGameFunc,
			validateLeaderboardFunc: validateLeaderboardFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"leaderboard":{"game_id":1,"name":"hiscore","version":1}}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/anumber/leaderboards/1", nil)
		r.SetPathValue("gameID", "anumber")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/0/leaderboards/1", nil)
		r.SetPathValue("gameID", "0")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/-10/leaderboards/1", nil)
		r.SetPathValue("gameID", "-10")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("game not found", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read game from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("leaderboardID is not numeric", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/anumber", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "anumber")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is zero", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/0", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "0")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is negative", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/-10", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "-10")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboard not found", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read leaderboard from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("gameID from endpoint does not match with gameID on leaderboard", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 100, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.getLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})
}

func TestDeleteLeaderboard(t *testing.T) {
	noopValidateGame := func(v *validator.Validator, game *data.Game) {}
	noopValidateLeaderboard := func(v *validator.Validator, game *data.Leaderboard) {}

	var buildApp = func(
		mLeaderboard data.LeaderboardsCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game),
		validateLeaderboardFunc func(_ *validator.Validator, _ *data.Leaderboard)) application {
		return application{
			config:                  config{},
			logger:                  slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:                  data.Models{Leaderboards: mLeaderboard},
			validateGameFunc:        validateGameFunc,
			validateLeaderboardFunc: validateLeaderboardFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Delete(1).Return(nil)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"message":"leaderboard successfully deleted"}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/anumber/leaderboards/1", nil)
		r.SetPathValue("gameID", "anumber")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/0/leaderboards/1", nil)
		r.SetPathValue("gameID", "0")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("DELETE", "/v1/games/-10/leaderboards/1", nil)
		r.SetPathValue("gameID", "-10")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboard not found", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("read leaderboard from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("leaderboardID is not numeric", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/anumber", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "anumber")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is zero", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/0", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "0")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboardID is negative", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/-10", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "-10")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("leaderboard not found", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Delete(1).Return(data.ErrRecordNotFound)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("delete leaderboard from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().Delete(1).Return(assert.AnError)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("GameID from endpoint dont match with gameID from Read call", func(t *testing.T) {
		// Setup
		mockL := data.NewMockLeaderboardsCRUDL(t)
		mockL.EXPECT().Read(mock.Anything).Return(&data.Leaderboard{ID: 1, Name: "hiscore", GameID: 100, Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("DELETE", "/v1/games/1/leaderboards/1", nil)
		r.SetPathValue("gameID", "1")
		r.SetPathValue("leaderboardID", "1")

		// Act
		app.deleteLeaderboard(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})
}

func TestListLeaderboards(t *testing.T) {
	noopValidateGame := func(v *validator.Validator, game *data.Game) {}
	noopValidateLeaderboard := func(v *validator.Validator, game *data.Leaderboard) {}

	var buildApp = func(
		mGame data.GamesCRUDL,
		mLeaderboard data.LeaderboardsCRUDL,
		validateGameFunc func(_ *validator.Validator, _ *data.Game),
		validateLeaderboardFunc func(_ *validator.Validator, _ *data.Leaderboard)) application {
		return application{
			config:                  config{},
			logger:                  slog.New(slog.NewTextHandler(io.Discard, nil)),
			models:                  data.Models{Games: mGame, Leaderboards: mLeaderboard},
			validateGameFunc:        validateGameFunc,
			validateLeaderboardFunc: validateLeaderboardFunc,
		}
	}

	t.Run("Happy path", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().List("", 1, mock.Anything).
			Return([]*data.Leaderboard{{ID: 1, Name: "hiscore", GameID: 1, Version: 1, CreatedAt: time.Now()}}, data.Metadata{}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards", nil)
		r.SetPathValue("gameID", "1")
		params := url.Values{}
		params.Add("page", "1")
		params.Add("page_size", "10")
		params.Add("sort", "id")
		r.URL.RawQuery = params.Encode()

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"leaderboards":[{"game_id":1,"name":"hiscore","version":1}], "metadata":{}}`, string(data))
	})

	t.Run("gameID is not numeric", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/anumber/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "anumber")

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is zero", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/0/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "0")

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("gameID is negative", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		// TODO: confirm -10 on postmant
		r := httptest.NewRequest("GET", "/v1/games/-10/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "-10")

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("game not found", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(nil, data.ErrRecordNotFound)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		assertNotFound(t, w.Result())
	})

	t.Run("Filter verification fails", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards", nil)
		r.SetPathValue("gameID", "1")
		params := url.Values{}
		params.Add("page", "-11")
		params.Add("page_size", "10")
		params.Add("sort", "id")
		r.URL.RawQuery = params.Encode()

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		assertUnprocessableEntity(t, w.Result(), `{"error":{"page":"must be greater than zero"}}`)
	})

	t.Run("read game from storage return unexpected value", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(nil, assert.AnError)
		// mockL.EXPECT().Create(mock.Anything).Return(nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards", strings.NewReader(`{"name": "hiscore"}`))
		r.SetPathValue("gameID", "1")

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})

	t.Run("Empty list", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().List("", 1, mock.Anything).
			Return([]*data.Leaderboard{}, data.Metadata{}, nil)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards", nil)
		r.SetPathValue("gameID", "1")

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		res := w.Result()
		defer res.Body.Close()
		data, err := io.ReadAll(res.Body)
		require.NoError(t, err)
		require.Equal(t, http.StatusOK, res.StatusCode)
		assert.JSONEq(t, `{"leaderboards":[], "metadata":{}}`, string(data))
	})

	t.Run("storage List call return un expected error", func(t *testing.T) {
		// Setup
		mockG, mockL := data.NewMockGamesCRUDL(t), data.NewMockLeaderboardsCRUDL(t)
		mockG.EXPECT().Read(mock.Anything).Return(&data.Game{ID: 1, Name: "testgame", Version: 1, CreatedAt: time.Now()}, nil)
		mockL.EXPECT().List("", 1, mock.Anything).
			Return(nil, data.Metadata{}, assert.AnError)
		app := buildApp(mockG, mockL, noopValidateGame, noopValidateLeaderboard)
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/v1/games/1/leaderboards", nil)
		r.SetPathValue("gameID", "1")

		// Act
		app.listLeaderboards(w, r)

		// Assert
		w.Flush()
		assertInternalServerError(t, w.Result())
	})
}
